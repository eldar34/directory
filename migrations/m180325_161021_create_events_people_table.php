<?php

use yii\db\Migration;

/**
 * Handles the creation of table `events_people`.
 */
class m180325_161021_create_events_people_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('events_people', [
            'id' => $this->primaryKey(),
            'events_id' => $this->integer(),
            'people_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('events_people');
    }
}
