<?php

use yii\db\Migration;

/**
 * Handles the creation of table `files`.
 */
class m180323_155213_create_files_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('files', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'size' => $this->string(255)->defaultValue('DIR'),
            'type' => $this->string(255)->null(),
            'datem' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('files');
    }
}
