<?php

use yii\db\Migration;

/**
 * Handles the creation of table `events`.
 */
class m180324_163731_create_events_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('events', [
            'id' => $this->primaryKey(),
            'event' => $this->string(),
            'begin_date' => $this->date(),
            'end_date' => $this->date(),
            'city_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('events');
    }
}
