<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\MySkol;
use app\models\Events;
use app\models\People;
use app\models\Cities;
use yii\helpers\ArrayHelper;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
  

     public function actionSixth()
    {
        return $this->render('sixth');
    }


     public function actionSeven()
    {
        $files = "Hello";
        $model = new MySkol;

        return $this->render('seven', [
            'files'=>$files,
            'model' => $model,
            
        ]);
    }

   

    public function actionUpdateall(){
            

            $myvar = new MySkol();
            $myvar->Updategt();
          
            return $this->redirect(['/files/index']);
        

    }

     public function actionFifth()
    {
        $model = new Events();
        $model2 = new People();
        $model3 = new Cities();
       

       
        $ju = [];
        $ju2 = [];
        $ju3 = [];
        
        $options = $model::find()->all();
        $options2 = $model2::find()->all();
         $fullname = [];

        foreach ($options2 as $item) {
            $fullname[]=[
                    'id'=>$item->id,
                    'fullname'=>$item->name.' '.$item->surname,
            ];
        }
        $options3 = $model3::find()->all();
        

        

        
        return $this->render('fifth', [
            'ju'=>$ju,
            'ju2'=>$ju2,
            'ju3'=>$ju3,
            'model'=>$model,
            'model2'=>$model2,
            'model3'=>$model3,
            'options' => $options,
            'options3' => $options3,
            'fullname' => $fullname,
        ]);
    }



     public function actionForpeople()
            {

        $model = new Events();
        $model2 = new People();
        $model3 = new Cities();
       

       
        $ju = [];
        $ju2 = [];
        $ju3 = [];
        
        $options = $model::find()->all();
        $options2 = $model2::find()->all();
         $fullname = [];

        foreach ($options2 as $item) {
            $fullname[]=[
                    'id'=>$item->id,
                    'fullname'=>$item->name.' '.$item->surname,
            ];
        }
        $options3 = $model3::find()->all();

        if(Yii::$app->request->post()){
            $event = $_POST['People']['name'];

            
            

            $gu = People::findOne($event);
            $gu2 = $gu->event;
            $ju2 = ArrayHelper::getColumn($gu2, 'event');

         
        }

        
        return $this->render('fifth', [
             'ju'=>$ju,
            'ju2'=>$ju2,
            'ju3'=>$ju3,
            'model'=>$model,
            'model2'=>$model2,
            'model3'=>$model3,
            'options' => $options,
            'options3' => $options3,
            'fullname' => $fullname,
        ]);
    }

    public function actionForevents()
            {

       $model = new Events();
        $model2 = new People();
        $model3 = new Cities();
       

       
        $ju = [];
        $ju2 = [];
        $ju3 = [];
        
        $options = $model::find()->all();
        $options2 = $model2::find()->all();
         $fullname = [];

        foreach ($options2 as $item) {
            $fullname[]=[
                    'id'=>$item->id,
                    'fullname'=>$item->name.' '.$item->surname,
            ];
        }
        $options3 = $model3::find()->all();
        

        if(Yii::$app->request->post()){
            $event = $_POST['Events']['event'];
            

            $gu = Events::findOne($event);
            $gu2 = $gu->people;
            $ju = ArrayHelper::map($gu2, 'name', 'surname');

         
        }

        
        return $this->render('fifth', [
            'ju'=>$ju,
            'ju2'=>$ju2,
            'ju3'=>$ju3,
            'model'=>$model,
            'model2'=>$model2,
            'model3'=>$model3,
            'options' => $options,
            'options3' => $options3,
            'fullname' => $fullname,
        ]);
    }

    public function actionForcities()
            {

        $model = new Events();
        $model2 = new People();
        $model3 = new Cities();
       

       
        $ju = [];
        $ju2 = [];
        $ju3 = [];
        
        $options = $model::find()->all();
        $options2 = $model2::find()->all();
         $fullname = [];

        foreach ($options2 as $item) {
            $fullname[]=[
                    'id'=>$item->id,
                    'fullname'=>$item->name.' '.$item->surname,
            ];
        }
        $options3 = $model3::find()->all();

        if(Yii::$app->request->post()){
            $event = $_POST['Cities']['city'];
            

            $gu = Events::find()->where(['city_id' => $event])->all();
            /*var_dump($gu);die;
            $gu2 = $gu->event;*/
            $ju3 = ArrayHelper::getColumn($gu, 'event');

         
        }

        
        return $this->render('fifth', [
            'ju'=>$ju,
            'ju2'=>$ju2,
            'ju3'=>$ju3,
            'model'=>$model,
            'model2'=>$model2,
            'model3'=>$model3,
            'options' => $options,
            'options3' => $options3,
            'fullname' => $fullname,
        ]);
    }




    /**
     * Login action.
     *
     * @return Response|string
     */
   
}
