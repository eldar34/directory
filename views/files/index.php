<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FilesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Files';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="files-index">
     <h4>В таблице содержутся все файлы хранящиеся в корневой папке хостинга. Можно создавать файлы/папки. После обновления они появятся в списке.</h4>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
        
         <?= Html::a('Create File', ['myfile'], ['class' => 'btn btn-warning']) ?>
         <?= Html::a('Create Folder', ['myfolder'], ['class' => 'btn btn-success']) ?>
    
  <?php /*Pjax::begin();*/ ?>
  <?php /*Pjax::end();*/ ?>
  
    <?= Html::a(
        'Update',
        ['/site/updateall'],
        ['class' => 'btn btn-info']
    ) ?>
    

<?php Pjax::begin(['id' => 'pjax-container']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            /*['class' => 'yii\grid\SerialColumn'],*/

            'id',
            'name',
            'size',
            'type',
            'datem',

            

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'onclick' => "
                                if (1) {
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        $.pjax.reload({container: '#pjax-container'});
                                    });
                                }
                                return false;
                            ",
                        ]);
                    },
                ],
            ],


            
        ],
    ]); ?>

<?php Pjax::end(); ?>

</div>
