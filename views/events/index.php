<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dosamigos\datepicker\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>

<h4>Можно добовлять в таблицу (событие + дата начала/окончания) и осуществлять поиск по промежутку дат.</h4>

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-12"  
    style="display:flex; flex-direction:row; align-items:flex-start; justify-content:space-around;">
<div class="col-md-8">
    <!-- This link created by standard gii generator -->
<h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Create Events', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
<div class="col-md-12" 
style="display:flex; flex-direction:row; align-items:flex-start; justify-content:space-around;
flex-wrap:wrap;
"
>
<?= $form->field($searchModel, 'begin_date')->label('From')->widget(
    DatePicker::className(), [
    // inline too, not bad
    'inline' => true,
    // modify template for custom rendering
    'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
    'clientOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
    ]
]);?>

<?= $form->field($searchModel, 'end_date')->label('To')->widget(
    DatePicker::className(), [
    // inline too, not bad
    'inline' => true,
    // modify template for custom rendering
    'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
    'clientOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
    ]
]);?>



<div class="col-md-11">
   <?= Html::submitButton('Search', ['class'=>'btn btn-info']) ?> 
</div>
    
    
    </div>

</div>

<br>
<?php ActiveForm::end(); ?>

<div class="events-index">

    

<?php Pjax::begin(['id' => 'pjax-container']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            

            'id',
            'event',
            'begin_date',
            'end_date',
            'city_id',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'delete' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', '#', [
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'onclick' => "
                                if (1) {
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        $.pjax.reload({container: '#pjax-container'});
                                    });
                                }
                                return false;
                            ",
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
