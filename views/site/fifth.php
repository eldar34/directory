<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

?>



<!-- Begin block for events -->

<?php Pjax::begin(); ?>


 <div class="col-md-4">
 	<?= Html::img('@web/img/first_t.png', ['alt' => 'First']) ?>
 
 <?php
   	$form = ActiveForm::begin([
   		'action' =>['site/forevents'], 'method' => 'post',
   	]);
    $items = ArrayHelper::map($options,'id','event');
    $params = [
        'prompt' => 'Выберите событие...'
    ];
   ?>
   <?= $form->field($model, 'event')->dropDownList($items,$params) ?>
<?= Html::submitButton('Search', ['class'=>'btn btn-info']) ?>  
   <?php ActiveForm::end(); ?>

<?php

echo '<br>';
$i = 1;
foreach ($ju as $key => $value) {
	echo $i.'. '.$key.' '.$value.'<br>';
	$i++;
}
?>
</div>

   <?php Pjax::end(); ?>

   <!-- End block for events -->

   <!-- Begin block for people -->

   <?php Pjax::begin(); ?>
 <div class="col-md-4">
 	<?= Html::img('@web/img/second_t.png', ['alt' => 'First']) ?>
 
 <?php
   	$form = ActiveForm::begin([
   		'action' =>['site/forpeople'], 'method' => 'post',
   	]);
    $items = ArrayHelper::map($fullname, 'id', 'fullname');
    $params = [
        'prompt' => 'Выберите участника...'
    ];
   ?>
   <?= $form->field($model2, 'name')->dropDownList($items,$params) ?>
<?= Html::submitButton('Search', ['class'=>'btn btn-info']) ?>  
   <?php ActiveForm::end(); ?>

<?php

echo '<br>';
$i = 1;
foreach ($ju2 as $key => $value) {
	echo $i.'. '.' '.$value.'<br>';
	$i++;
}

?>
</div>

   <?php Pjax::end(); ?>

   <!-- End block for people -->

   <!-- Begin block for cities -->

      <?php Pjax::begin(); ?>
 <div class="col-md-4">
    <?= Html::img('@web/img/third_t.png', ['alt' => 'First']) ?>
 
 <?php
    $form = ActiveForm::begin([
      'action' =>['site/forcities'], 'method' => 'post',
    ]);
    $items = ArrayHelper::map($options3, 'id', 'city');
    $params = [
        'prompt' => 'Выберите город...'
    ];
   ?>
   <?= $form->field($model3, 'city')->dropDownList($items,$params) ?>
<?= Html::submitButton('Search', ['class'=>'btn btn-info']) ?>  
   <?php ActiveForm::end(); ?>

<?php

echo '<br>';
$i = 1;
foreach ($ju3 as $item) {
  echo $i.'. '.' '.$item.'<br>';
  $i++;
}

?>
</div>

   <?php Pjax::end(); ?>
<div class="col-md-12"><br><br>
  <h1>Задание:<h1>
     <h4>Каждый семинар характеризуется следующими атрибутами:<br> название, дата начала, дата
окончания, город, участники события.<br> Необходимо спроектировать структуру таблиц БД для
хранения записей о таких семинарах.</h4>

<h1>Решение:<h1>

<h4>
 Создаём таблицу(events) в которой будем хранить информацию о событиях(id, название, дата начала, дата окончания, city_id).
</h4>
<?= Html::img('@web/img/first_t.png', ['alt' => 'First']) ?>
<h4>
Далее создаём таблицу для участников события(people), в которой будем хранить информацию об участниках события(id, имя, фамилия).
</h4>
<?= Html::img('@web/img/second_t.png', ['alt' => 'First']) ?>
<h4>
 Далее создаём таблицу для городов в которых происходили события(cities), в которой будем хранить информацию о городах(id, название города).
</h4>
<?= Html::img('@web/img/third_t.png', ['alt' => 'First']) ?>
<h4>
 Создаём промежуточную таблицу для связей (events_people), для получения информации о том кто из участников посетил событие. И на каких событиях присутствовал участник. В таблице(events_people), будем хранить поля(id, events_id, people_id).
</h4>
<?= Html::img('@web/img/fourth_t.png', ['alt' => 'First']) ?>
<h4>
Далее настраиваем модели в Yii framework. Модель промежуточной таблицы для связей EVENTS_PEOPLE
</h4>
<?= Html::img('@web/img/first_c.png', ['alt' => 'First']) ?>
<h4>
Связи модели EVENTS
</h4>
<?= Html::img('@web/img/second_c.png', ['alt' => 'First']) ?>
<h4>
Связь модели PEOPLE 
</h4>
<?= Html::img('@web/img/third_c.png', ['alt' => 'First']) ?>
<h4>
Связь модели CITIES 
</h4>
<?= Html::img('@web/img/fourth_c.png', ['alt' => 'First']) ?>




 </div>   

   <!-- End block for cities -->




