<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\FileHelper;
use app\models\MySkol;
use yii\widgets\Pjax;
use dosamigos\datepicker\DatePicker;
use yii\widgets\ActiveForm;

echo $files;
?>



<?php $form = ActiveForm::begin(); ?>
<div class="col-md-8 row"  style="display:flex; flex-direction:row; justify-content: space-around;">
<?= $form->field($model, 'name')->label('From')->widget(
    DatePicker::className(), [
    // inline too, not bad
    'inline' => true,
    // modify template for custom rendering
    'template' => '<div class="input-group input-daterange" style="background-color: #fff; width:250px">{input}</div>',
    'clientOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
    ]
]);?>

<?= $form->field($model, 'size')->label('To')->widget(
    DatePicker::className(), [
    // inline too, not bad
    'inline' => true,
    // modify template for custom rendering
    'template' => '<div class="input-group input-daterange" style="background-color: #fff; width:250px">{input}</div>',
    'clientOptions' => [
        'autoclose' => true,
        'format' => 'yyyy-mm-dd'
    ]
]);?>
</div>


<div class="col-md-12"><br>
    <?= Html::submitButton('Search', ['class'=>'btn btn-success']) ?>
    <?=Html::tag('br', Html::encode(' '), ['class' => 'visitors-index'])?>
</div><br>
<?php ActiveForm::end(); ?>



