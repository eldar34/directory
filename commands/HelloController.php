<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Cities;
use app\models\People;
use app\models\Events;
use app\models\EventsPeople;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionSeeds()
    {
        $citiesCount = Cities::find()->count();
        
        if($citiesCount == 0){
            $capitals = $this->actionData('capitals');
            foreach($capitals as $capital){
                $model = new Cities();
                $model->city = $capital;
                if(!$model->save()){
                    var_dump($model->errors);
                    break;
                }
            }
        }
        

        $personsCount = People::find()->count();

        if($personsCount == 0){
            $persons = $this->actionData('person');

            foreach($persons as $name => $surename){
                $model = new People();
                $model->name = $name;
                $model->surname = $surename;
                if(!$model->save()){
                    var_dump($model->errors);
                    break;
                }
            }
        }

        $eventsCount = Events::find()->count();

        if($eventsCount == 0){
            $events = $this->actionData('event');

            foreach($events as $event){
                $model = new Events();
                $model->event = $event['title'];
                $model->begin_date = $event['begin_date'];
                $model->end_date = $event['end_date'];
                $model->city_id = $event['city_id'];
                if(!$model->save()){
                    var_dump($model->errors);
                    break;
                }
            }
        }

        $linktableCount = EventsPeople::find()->count();

        if($linktableCount == 0){
            $linktable = $this->actionData('links');

            foreach($linktable as $events_id => $people_id){
                $model = new EventsPeople();
                $model->events_id = $events_id;
                $model->people_id = $people_id;
                if(!$model->save()){
                    var_dump($model->errors);
                    break;
                }
            }
        }  

    }

    private function actionData($datatype)
    {
        $persons = [
            'John' => 'Smith', 
            'Jane' => 'Clight',
            'Emily' => 'Brown',
            'Sarah' => 'Harris',
            'Henry' => 'White',
            'Frankie' => 'Morris',
            'Elliott' => 'Ross'
        ];

        $events = [
            [
                'title'=>'First',
                'begin_date' => '2018-03-04',
                'end_date' => '2018-03-11',
                'city_id' => 1
            ],
            [
                'title'=>'Second',
                'begin_date' => '2018-03-05',
                'end_date' => '2018-03-12',
                'city_id' => 1
            ],
            [
                'title'=>'Third',
                'begin_date' => '2018-03-06',
                'end_date' => '2018-03-13',
                'city_id' => 2
            ],
            [
                'title'=>'Fourth',
                'begin_date' => '2018-03-07',
                'end_date' => '2018-03-14',
                'city_id' => 3
            ],
            [
                'title'=>'Fifth',
                'begin_date' => '2018-03-08',
                'end_date' => '2018-03-15',
                'city_id' => 2
            ],
            [
                'title'=>'Sixth',
                'begin_date' => '2018-03-09',
                'end_date' => '2018-03-16',
                'city_id' => 3
            ],
            [
                'title'=>'Seventh',
                'begin_date' => '2018-03-10',
                'end_date' => '2018-03-17',
                'city_id' => 2
            ],
        ];

        $linktable = [
            '1' => '1',
            '2' => '1',
            '1' => '2',
            '3' => '5',
            '4' => '7',
            '5' => '6',
            '7' => '5',
            '7' => '3'
        ];

        $capitals = ['Moscow', 'Paris', 'London'];

        switch($datatype){
            case 'person':
                $data = $persons;
            break;
            case 'event':
                $data = $events;
            break;
            case 'links':
                $data = $linktable;
            break;
            default:
                $data = $capitals;
        }

        return $data;
    }
}
