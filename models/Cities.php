<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Cities".
 *
 * @property int $id
 * @property string $city
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city'], 'required'],
            [['city'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city' => 'City',
        ];
    }

    public function getEvent()
    {
        return $this->hasMany(Events::className(), ['city_id' => 'id']);
    }
}
