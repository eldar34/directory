<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Events".
 *
 * @property int $id
 * @property string $event
 * @property string $begin_date
 * @property string $end_date
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['begin_date', 'end_date', 'event'], 'required'],
            [['city_id'], 'integer'],
            [['begin_date', 'end_date'], 'safe'],
            [['event'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event' => 'Event',
            'begin_date' => 'Begin Date',
            'end_date' => 'End Date',
            'city_id' => 'City Id',
        ];
    }

     public function getPeople()
    {
        return $this->hasMany(People::className(), ['id' => 'people_id'])
            ->viaTable('events_people', ['events_id' => 'id']);
    }

     public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }
}
