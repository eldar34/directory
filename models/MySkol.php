<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;
use yii\i18n\Formatter;
use yii\helpers\FileHelper;


class MySkol extends ActiveRecord{

    public function rules()
    {
        return [
            [['name', 'size', 'type', 'datem'], 'string', 'max' => 255],
            ['name', 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9ё\s,]+$/u'],
            ['size', 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9ё\s,]+$/u'],
        ];
    }
    
    public static function tableName(){
        
        
        return 'files';
    }
/*Function for get File Size*/
     public static function getFileSize( $path ){
        $fileSize   = filesize($path) ;
        return \Yii::$app->formatter->asShortSize( $fileSize ) ;
    }
/*Function for get Fiile Extension*/
    public static function getExtension2($filename) {
                  $path_info = pathinfo($filename, PATHINFO_EXTENSION);
                  return $path_info;
                  }
/*Function for get File Name*/
    public static function getFileName($filename) {
                  $path_info = pathinfo($filename, PATHINFO_BASENAME);
                  return $path_info;
                  }
/*Function for get Date of File Changes*/
    public static function getDateM($filename) {
                  $forreal = filemtime($filename) + 10800;
                  $path_info = date ("F d Y H:i:s.", $forreal);
                  return $path_info;
                  }

     public static function Updategt(){

      $model = MySkol::find()->all();
/*If Table have any rows, delete all*/
      if($model){

        Yii::$app->db->createCommand()->truncateTable('files')->execute();
      }

/*Else Select all files and folders on webroot directory*/

        $way = Yii::getAlias('@app');

        $files = FileHelper::findFiles( $way, ['recursive'=>FALSE]);
        $folders = FileHelper::findDirectories ( $way, ['recursive'=>FALSE]);

        
/*With foreach add all folders in table(files)*/ 

        foreach ($folders as $item) {
            $data = new MySkol();
           /*$data::update(['name'=> self::getFileName($item)]);*/
          
          $data->name = self::getFileName($item);
          $data->datem = self::getDateM($item);

          $data->save(false);
          } 

/*With foreach add all files in table(files)*/ 

          foreach ($files as $item) {
            $data = new MySkol();
           /*$data::update(['name'=> self::getFileName($item)]);*/
          
          $data->name = self::getFileName($item);
          $data->size = self::getFileSize($item);
          $data->type = self::getExtension2($item);
          $data->datem = self::getDateM($item);
          
          $data->save(false);
          }
    }
/*Create Files*/
    public static function FilesAdd($params){

          $name = $params['MySkol']['name'];
          $content = $params['MySkol']['size'];

          
          $webroot = Yii::getAlias('@app');
          $file =  $webroot . DIRECTORY_SEPARATOR . $name .'.txt';
          $handle = fopen($file, 'w');
          fwrite($handle, $content);
          fclose($handle);
    }

 /*Create Folders*/

  public static function FoldersAdd($params){

          $name = $params['MySkol']['name'];
          

          
          $webroot = Yii::getAlias('@app');
          $file =  $webroot . DIRECTORY_SEPARATOR . $name;
          FileHelper::createDirectory($file, $mode = 0775, $recursive = false);
          
    }
    
    public static function getAll(){
         
        $data = self::find()->all();
        return $data;
    }
    
    public static function getOne($id){
        
        $data = self::find()
            ->where(['id'=>$id])
            ->one();
        return $data;
        
    }
        
        
    
}

?>