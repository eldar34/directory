<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Files".
 *
 * @property int $id
 * @property string $name
 * @property string $size
 * @property string $type
 * @property string $datem
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'size', 'type', 'datem'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'size' => 'Size',
            'type' => 'Type',
            'datem' => 'Datem',
        ];
    }
}
