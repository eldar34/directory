<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "People".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 */
class People extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'people';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname'], 'required'],
            [['name', 'surname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
        ];
    }

     public function getEvent()
    {
        return $this->hasMany(Events::className(), ['id' => 'events_id'])
            ->viaTable('events_people', ['people_id' => 'id']);
    }
}
